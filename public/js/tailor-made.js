
$(document).ready(function () {

    $(window).resize(function () {
        resizeValign();
    });

    resizeValign();

    const catTissu = $("#selectTissu");
    const catVoilage = $("#selectVoilage");
    const informations = $('.informationBloc');

    $(catTissu).append("<option disabled selected>Veuillez choisir le type de tissu</option>");
    $(catTissu).trigger('contentChanged');
    $(catVoilage).append("<option disabled selected>Veuillez choisir le type de tissu</option>");
    $(catVoilage).trigger('contentChanged');

    $('.catTissu').click(function () {
        const id = $(this).val();

        $.ajax({
            url: './controller/switch.php',
            type: 'POST',
            //async: false,

            data: {
                action: 'curtains',
                id: id
            },

            dataType: 'json',

            success: function (data) {

                $(catTissu)
                    .find('option')
                    .remove()
                    .end();
                $(informations).empty().end();


                $(catTissu).append($("<option disabled selected>Choix du tissu</option>"));

                $.each(data, function (index, item) {

                    const option = $('<option>', {
                        value: item[1],
                        text: item[1]
                    });

                    $(catTissu).append(option);

                    $(catTissu).trigger('contentChanged');

                    $(informations).append($(
                        "<div class='card horizontal userSelect'>" +
                            "<div class='card-image'>" +
                                "<img src='../public/images/curtains.png' alt='curtains' />" +
                            "</div>" +

                            "<div class='card-stacked'>" +
                                "<div class='card-content'>" +
                                    "<p>" + item[1] + "</p>" +
                                "</div>" +
                            "</div>" +
                        "</div>"
                    ));
                });
            }
        });
    });

    $('.catVoilage').click(function () {
        const id = $(this).val();

        $.ajax({
            url: './controller/switch.php',
            type: 'POST',
            //async: false,

            data: {
                action: 'sheers',
                id: id
            },

            dataType: 'json',

            success: function (data) {

                $(catVoilage)
                    .find('option')
                    .remove()
                    .end();
                $(informations).empty().end();

                $(catVoilage).append($("<option disabled selected>Choix du tissu</option>"));

                $.each(data, function (index, item) {
                    const option = $('<option>', {
                        value: item[0],
                        text: item[1]
                    });

                    $(catVoilage).append(option);

                    $(catVoilage).trigger('contentChanged');

                    $(informations).append($(
                        "<div class='card horizontal userSelect'>" +
                            "<div class='card-image'>" +
                                "<img src='../public/images/curtains.png' alt='curtains' />" +
                            "</div>" +

                            "<div class='card-stacked'>" +
                                "<div class='card-content'>" +
                                    "<p>" + item[1] + "</p>" +
                                "</div>" +
                            "</div>" +
                        "</div>"
                    ));



                });

                $('.select-wrapper').change('select', function () {
                    const idTypeVoilage = $(catVoilage).val();

                    if (idTypeVoilage !== null) {

                    $.ajax({
                        url: './controller/switch.php',
                        type: 'POST',

                        data: {
                            action: 'sheersDetails',
                            id: idTypeVoilage
                        },

                        dataType: 'json',

                        success: function (details) {
                            console.log(details);

                            $.each(details, function (index, value) {

                                if (value[0] === "1") {
                                    $('#hiddenOurlet').html(
                                        "<br><br><br>" +
                                            "<div>" +
                                                "<p class='inline'>Choix de l'ourlet : </p>" +

                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +

                                                "<label class='black-text'>" +
                                                    "<input type='radio' name='ourlet'>" +
                                                    "<span>Plombé</span>" +
                                                "</label>" +

                                                "&nbsp;&nbsp;&nbsp;" +

                                                "<label class='black-text'>" +
                                                    "<input type='radio' name='ourlet'>" +
                                                    "<span>Pli</span>" +
                                                "</label>" +
                                        "</div>");
                                }
                                else if(value[0] === "0") {
                                    $('#hiddenOurlet').html("<br><br>");
                                }
                            });
                        }
                    });
                    }

                });
            }
        });
    });
});