$(document).ready(function () {
    // MISE A JOUR DES INFORMATIONS GÉNÉRALES
    $('#updateGeneral').click('.infGeneral[data-editable]', function () {
        // ACTIVATION DE LA MODIFICATION
            // On récupère tous les éléments span de la classe .infGeneral que l'on transforme en input
        $('.infGeneral').each(function() {
            const text = $(this);
            const input = $('<input style="width: 15%;">', {
                type: 'text',
                class: 'upGeneral',
                value: text.text()
            });

                // On remplace le bouton de modification par le bouton d'enregistrement
            $('#updateGeneral').replaceWith('<button class="btn waves-effect waves-light" id="updatedGeneral" data-id="' + dataId + '">Enregistrer</button>');
            text.replaceWith(input);
        });

        // ACTIVATION DE L'ENREGISTREMENT
            // On retransforme les inputs en span avec une génération d'id pour récupérer les nouvelles valeurs
        $('#updatedGeneral').click('.upGeneral', function () {
            const general = $('.upGeneral');

            for (let i = 1; i < $(general).length;) {
                $(general).each(function() {
                    const input = $(this);
                    const span = $('<span>', {
                        class: 'infGeneral',
                        id: 'general' + i++,
                        text: input.val()
                    });

                        // On remplace le bouton d'enregistrement par le bouton de modification
                    $('#updatedGeneral').replaceWith('<button class="btn waves-effect waves-light" id="updateGeneral">Modifier</button>');
                    input.replaceWith(span);
                });
            }

                // On récupère les données
                const id_user = $(this).attr('data-id');
                const lastname = $('#general1').text();
                const firstname = $('#general2').text();
                const address = $('#general3').text();
                const city = $('#general4').text();
                const cp = $('#general5').text();
                const phone = $('#general6').text();


                // On envoie les informations en AJAX en type POST
                $.ajax({
                    url: './controller/switch.php',
                    type: 'POST',
                    async: false,
                    data: {
                        id: id_user,
                        lastname: lastname,
                        firstname: firstname,
                        address: address,
                        city: city,
                        cp: cp,
                        phone: phone,
                        action: 'updateGeneral'
                    }
                })
                    .done(function () {
                        setTimeout(function () {
                            location.reload();
                        }, 5703);

                        M.toast({
                            html: "<span>Le profil a été mis à jour</span>" +
                            "<button class='btn-flat toast-action' onclick='closeToast();'>Fermer</button>",
                            displayLength: 5000,
                            inDuration: 200,
                            outDuration: 500,
                            classes: 'toastPosition'
                        });

                    })
            });
    });

    // MISE A JOUR DES INFORMATIONS DE L'ENTREPRISE
    $('#updateCompany').click('.infCompany[data-editable]', function () {
        // Même fonctionnement que le précédent

        $('.infCompany').each(function() {
            const text = $(this);
            const input = $('<input style="width: 15%;">', {
                type: 'text',
                class: 'upCompany',
                value: text.text()
            });

            $('#updateCompany').replaceWith('<button class="btn waves-effect waves-light" id="updatedCompany" data-id="' + dataId + '">Enregistrer</button>');
            text.replaceWith(input);
        });

        $('#updatedCompany').click('.upCompany', function () {
            const upCompany = $('.upCompany');

            for (let i = 1; i < $(upCompany).length;) {
                $(upCompany).each(function() {
                    const input = $(this);
                    const span = $('<span>', {
                        class: 'infCompany',
                        id: 'company' + i++,
                        text: input.val()
                    });

                    $('#updatedGeneral').replaceWith('<button class="btn waves-effect waves-light" id="updateCompany">Modifier</button>');
                    input.replaceWith(span);
                });
            }

            const id_user = $(this).attr('data-id');
            const company = $('#company1').text();
            const type_comp = $('#company2').text();

            $.ajax({
                url: './controller/switch.php',
                type: 'POST',
                async: false,
                data: {
                id: id_user,
                company: company,
                type_comp: type_comp,
                action: 'updateCompany'
            }})
                .done(function () {
                    setTimeout(function () {
                        location.reload();
                    }, 5703);

                    M.toast({
                        html: "<span>Les informations sur l'entreprises ont été mis à jour</span>" +
                        "<button class='btn-flat toast-action' onclick='closeToast();'>Fermer</button>",
                        displayLength: 5000,
                        inDuration: 200,
                        outDuration: 500,
                        classes: 'toastPosition'
                    });
                })
        });
    });

    // MISE A JOUR DES IDENTIFIANTS DE CONNEXION
    $('#updateConnexion').click('.infConnexion[data-editable]', function () {
        // Même fonctionnement que les deux précédents sauf que l'on récupère qu'une donnée au départ et on en rajoute 3

        const text = $('.infConnexion');
        const input = $('<input style="width: 10%;">', {
            type: 'text',
            class: 'upConnexion',
            value: text.text()
        });

        // On rajoute la modification du mot de passe
        /*text.after(
            "<div id='updatePassword'>" +
                "<br><br>" +
                "<b>Ancien mot de passe : </b><input type='password' class='upConnexion' placeholder='Ancien mot de passe'>" +
                "<br><br>" +
                "<b>Nouveau mot de passe : </b><input type='password' class='upConnexion' placeholder='Nouveau mot de passe'>" +
                "&nbsp;&nbsp;&nbsp;&nbsp;" +
                "<input type='password' class='upConnexion' placeholder='Confirmer le mot de passe'>" +
            "</div>"
        );*/

        text.after(
            '<div id="updatePassword">' +
                '<br><br>' +

                '<div class="input-field inline">\n' +
            '       <i class="material-icons prefix userSelect">enhanced_encryption</i>\n' +
            '       <input type="password" class="validate upConnxeion" id="old_pwd">\n' +
            '       <label for="old_log" class="userSelect">Ancien mot de passe</label>\n' +
            '       <span class="helper-text" data-error="" data-success=""></span>\n' +
            '    </div>' +

                '<br>' +

                '<div class="input-field inline">\n' +
            '       <i class="material-icons prefix userSelect">enhanced_encryption</i>\n' +
            '       <input type="password" name="password" class="validate" id="new_pwd">\n' +
            '       <label for="new_pwd" class="userSelect">Nouveau mot de passe</label>\n' +
            '       <span class="helper-text" data-error="" data-success=""></span>\n' +
            '    </div>' +

                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +

                '<div class="input-field inline">\n' +
            '       <input type="password" name="password" class="validate" id="confNew_pwd">\n' +
            '       <label for="confNew_pwd" class="userSelect">Confirmer le mot de passe</label>\n' +
            '       <span class="helper-text" data-error="" data-success=""></span>\n' +
            '    </div>' +

            '</div>'
        );

            $('#updateConnexion').replaceWith('<button class="btn waves-effect waves-light" id="updatedConnexion" data-id="' + dataId + '">Enregistrer</button>');
            text.replaceWith(input);

        $('#updatedConnexion').click('.upConnexion', function () {
            const upConnexion = $('.upConnexion');

            for (let i = 1; i < $(upConnexion).length;) {
                $(upConnexion).each(function() {
                    const input = $(this);
                    const span = $('<span />', {
                        class: 'infConnexion',
                        id: 'connexion' + i++,
                        text: input.val()
                    });

                    $('#updatedConnexion').replaceWith('<button class="btn waves-effect waves-light" id="updateConnexion">Modifier</button>');
                    input.replaceWith(span);
                });
            }

            const id_user = $(this).attr('data-id');
            const email = $('#connexion1').text();
            const oldPwd = $('#connexion2').text();
            const newPwd1 = $('#connexion3').text();
            const newPwd2 = $('#connexion4').text();

            $.post('./controller/switch.php', {
                    id: id_user,
                    email: email,
                    oldPwd: oldPwd,
                    newPwd1: newPwd1,
                    newPwd2: newPwd2,
                    action: 'updateConnexion'
            })
                .done(function () {
                    setTimeout(function () {
                        location.reload();
                    }, 5703);

                    M.toast({
                        html: "<span>Les identifiants ont été mis à jour</span>" +
                        "<button class='btn-flat toast-action' onclick='closeToast();'>Fermer</button>",
                        displayLength: 5000,
                        inDuration: 200,
                        outDuration: 500,
                        classes: 'toastPosition'
                    });
                });

            // On supprime la div pour les mots de passe
            $('#updatePassword').remove();
        });
    });
});