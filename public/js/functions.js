function resizeValign() {
    if ($(window).width() > 588) {
        return $('div.TDvalign').addClass('valign-wrapper');
    }

    $('.TDvalign').removeClass('valign-wrapper');
}

function adaptHeight($class) {
    let maxHeight = 0;

    $($class).each(function(){
        if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
    });

    $($class).height(maxHeight);
}