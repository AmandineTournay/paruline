// Fonction ajax qui permet de rediriger dynamiquement vers la page ajax.php qui active
// la bonne action à faire en fonction du paramètre action.

$(document).ready(function () {

    // AJOUT D'UN PRODUIT AU PANIER
    $('button.addToCart').click(function (e) {
        // On récupère l'attribut data-id pour savoir quel produit se trouve affecté par l'action
        // On récupère l'attribut quantityID qui prend l'id de la quantité (généré par le foreach) et on lit sa valeur

        // Empêcher le rafraîchissement de la page à cause de l'ajax
        e.preventDefault();
        e.stopPropagation();

        const id_prod = $(this).attr('data-id');
        const quantityID = $(this).attr('quantityID');
        const quantity = $(quantityID).val();

        // On envoie les données en POST par l'ajax et les données sont récupérées de façon classique côté PHP
        // async: false permet à Firefox d'envoyer les données car sinon il n'agit pas en true
        // Le navigateur doit attendre le retour du traitement pour continuer
        $.ajax( {
            type: 'POST',
            url: './controller/switch.php',
            async: false,
            data: {
                id: id_prod,
                quantity: quantity,
                action: 'add'
            }
        })
            .done(function () {
                M.toast({
                    html: "<span>Le produit a bien été ajouté au panier</span>" +
                            "<button class='btn-flat toast-action' onclick='closeToast();'>Fermer</button>",
                    displayLength: 2500,
                    inDuration: 200,
                    outDuration: 400,
                    classes: 'toastPosition'
                });
            });
    });

    // MISE A JOUR DE LA QUANTITE
    $('.updateQuantity').keypress(function(e){
        // La fonction permet d'empêcher les sauts de ligne dans le contenteditable

        return e.which !== 13;
    });

    $('td[contenteditable=true]').focusout(function (e) {
        // On récupère l'attribut de l'id du contenteditable et on lit sa valeur
        // La façon de récupérer la valeur est différente car ce n'est pas une insertion de texte

        e.preventDefault();
        e.stopPropagation();

        const id_quan = $(this).attr("id");
        const updateQuantity = $(this).text();

        $('button.updateCart').click(function () {
            // Envoi des données pour mettre à jour le panier
            const id_prod = $(this).attr('data-id');

            $.post('./controller/switch.php', { id: id_prod, quantity: updateQuantity, action: 'update' }, function () {
                M.toast({
                    html: "<span>La quantité du produit a bien été mis à jour</span>" +
                    "<button class='btn-flat toast-action' onclick='closeToast();'>Fermer</button>",
                    displayLength: 2500,
                    inDuration: 200,
                    outDuration: 400,
                    classes: 'toastPosition'
                });
                window.setTimeout(function () { location.reload(); }, 2000);
            })
        });
    });

    // SUPPRESSION D'UN PRODUIT DU PANIER
    $('button.removeFromCart').click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        // Supprimer un produit du panier
        const id = $(this).attr('data-id');

        $.post('./controller/switch.php', { id: id, action: 'remove' }, function () {
            M.toast({
                html: "<span>Le produit a été supprimé</span>" +
                "<button class='btn-flat toast-action' onclick='closeToast();'>Fermer</button>",
                displayLength: 2500,
                inDuration: 200,
                outDuration: 400,
                classes: 'toastPosition'
            });
            window.setTimeout(function () { location.reload(); }, 2000);
        });
    });

    // VIDER DU PANIER
    $('button.emptyCart').click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        // Vider le panier
        $.post('./controller/switch.php', { action: 'empty' }, function () {
            M.toast({
                html: "<span>Le panier est vidé</span>" +
                "<button class='btn-flat toast-action' onclick='closeToast();'>Fermer</button>",
                displayLength: 2000,
                inDuration: 200,
                outDuration: 400,
                classes: 'toastPosition'
            });
            window.setTimeout(function () { location.reload(); }, 1200);
        });
    });
});