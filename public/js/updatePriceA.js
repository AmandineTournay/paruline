$(document).ready(function () {

    // MISE A JOUR DES PRIX DES ACCESSOIRES

    $("button.updatePriceAccess").click(function () {
        // RÉCUPÉRATION DES ATTRIBUTS, IDs et VALEURS

        const id = $(this).data('idprice');
        const idRef = $(this).attr('data-ref');
        const idPF = $(this).attr('data-fournis');
        const idCoeff = $(this).attr('data-coeff');

        const pRID = idRef.substring(1);
        const pfID = idPF.substring(1);
        const pCID = idCoeff.substring(1);

        const id_price = $(id).val();
        const ref = $(idRef).text();
        const pFourn = $(idPF).text();
        const pCoeff = $(idCoeff).text();


        // REMPLACEMENT PAR UN TEXTE ÉDITABLE
        $(idRef).replaceWith($('<input>', {
            type: 'text',
            id: 'newRef',
            value: ref
        }));
        $(idPF).replaceWith($('<input>', {
            type: 'number',
            min: 0,
            step: 0.01,
            id: 'newPF',
            value: pFourn
        }));
        $(idCoeff).replaceWith($('<input>', {
            type: 'number',
            min: 0,
            max: 50,
            step: 0.01,
            id: 'newCoeff',
            value: pCoeff
        }));

        $(this).replaceWith('<button class="btn waves-effect waves-light updatedPriceAccess">Enregistrer</button>');

        // LANCEMENT DE LA MISE A JOUR

        $('button.updatedPriceAccess').click(function () {
            const newIDPF = $('#newPF');
            const newIDRef = $('#newRef');
            const newIDCoeff = $('#newCoeff');

            const newPriceF = $(newIDPF).val();
            const newRef = $(newIDRef).val();
            const newPriceC = $(newIDCoeff).val();



            $(newIDPF).replaceWith("<span id='" + pfID + "'>" + newPriceF + "</span>");
            $(newIDRef).replaceWith("<span id='" + pRID + "'>" + newRef + "</span>");
            $(newIDCoeff).replaceWith("<span id='" + pCID + "'>" + newPriceC + "</span>");

            $(this).replaceWith("<button " +
                "data-fournis='" + idPF + "' data-ref='" + idRef + "' data-coeff='" + idCoeff + "'" +
                " class='btn waves-effect waves-light updatePriceAccess'>Modifier</button>");


            $.ajax({
                url: './controller/switch.php',
                type: 'POST',
                async: false,
                data: {
                    id: id_price,
                    fournisseur: newPriceF,
                    ref: newRef,
                    coeff: newPriceC,
                    action: 'updatePriceAcc'
                },
                success: function () {
                    M.toast({
                        html: "<span>Le prix a été mis à jour.</span>" +
                        "<button class='btn-flat toast-action' onclick='closeToast();'>Fermer</button>",
                        displayLength: 5000,
                        inDuration: 200,
                        outDuration: 500,
                        classes: 'toastPosition'
                    });

                    setTimeout(function () { location.reload(); }, 5703);
                }
            })
        })
    });
});