// Instantiation automatique des effets Javascript de Materialize

M.AutoInit(document.body);

M.FormSelect.init($('select'));
M.Sidenav.init($('.sidenav'));
M.Sidenav.init($('.sidenav-trigger'));
M.Collapsible.init($('.collapsible'));
M.Tabs.init($('ul.tabs'));


// IMAGES
window.ondragstart = function () { return false; };

// BOUTON POUR FERMER LES TOASTS
function closeToast() {
    const toast = M.Toast.getInstance($('.toast'));

    toast.dismiss();
}

$('select').on('contentChanged', function () {
    M.FormSelect.init($(this));
});

// EN CAS DE DIFFÉRENCE ENTRE LES NAVIGATEURS
$(function(){
    const userAgent = navigator.userAgent.toLowerCase();

    if (userAgent.indexOf("chrome") !== -1){

    }
    else {

    }
});