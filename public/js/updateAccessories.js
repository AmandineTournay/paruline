$(document).ready(function () {

    // MISE A JOUR DES ACCESSOIRES

   $('button.updateAccessories').click(function () {
       const idp = $(this).attr('data-idp');

       // RÉCUPÉRATION DES ATTRIBUTS
       const titleID = $(this).attr('data-titleID');
       const descriptionID = $(this).attr('data-descriptionID');

       // RÉCUPÉRATION DES IDs
       const titleSubstr = titleID.substring(1);
       const descriptionSubstr = descriptionID.substring(1);

       // RÉCUPÉRATION DES VALEURS DES IDs
       const id_product = $(idp).val();
       const title = $(titleID).text();
       const description = $(descriptionID).text();


       // TRANSFORMATION EN TEXTE ÉDIATBLE
       $(titleID).replaceWith($('<input>', {
           type: 'text',
           id: 'new' + titleSubstr,
           value: title
       }));
       $(descriptionID).replaceWith($('<textarea rows="2" cols="40" id="new' + descriptionSubstr + '">').val(description));

       $(this).replaceWith('<button class="btn waves-effect waves-light updateAccess updatedAccessories">Enregistrer</button>');

       // LANCEMENT DE LA MISE A JOUR
       $('button.updatedAccessories').click(function () {

           // RÉCUPÉRATION DES NOUVELLES VALEURS
           const newTitleID = $('#new' + titleSubstr);
           const newDescriptionID = $('#new' + descriptionSubstr);

           const newTitle = $(newTitleID).val();
           const newDescription = $(newDescriptionID).val();


           // RETOUR A L'AFFICHE EN TEXTE
           $(newTitleID).replaceWith($('<span id="' + titleID + '" class="activator">').text(newTitle));
           $(newDescriptionID).replaceWith($('<span id="' + descriptionID + '">').text(newDescription));

           $(this).replaceWith('<button class="btn waves-effect waves-light updateAccess updateAccessories" ' +
               'data-id="' + id_product + '" data-titleID="' + titleID + '" data-descriptionID="' + descriptionID + '">Modifier</button>');


           // ENVOI DES DONNÉES
           $.ajax({
               url: './controller/switch.php',
               type: 'POST',
               async: false,
               data: {
                   id: id_product,
                   title: newTitle,
                   description: newDescription,
                   action: 'updateAccessory'
               },
               success: function () {

                   // AFFICHAGE DU TOAST
                   M.toast({
                       html: "<span>L'accessoire a été mis à jour</span>" +
                       "<button class='btn-flat toast-action' onclick='closeToast();'>Fermer</button>",
                       displayLength: 5000,
                       inDuration: 200,
                       outDuration: 400,
                       classes: 'toastPosition'
                   });
                   window.setTimeout(function () { location.reload(); }, 1200);
               }
           })
       });
   });
});