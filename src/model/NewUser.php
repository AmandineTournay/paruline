<?php
    include_once "General.php";

    class NewUser {
        private static $_instance = null;
        private $bdd;

        private function __construct() {
            $this->bdd = General::getInstance()->connect("localhost", "paruline", "root", "");
        }

        public static function getInstance() {
            if (is_null(self::$_instance)) {
                self::$_instance = new NewUser();
            }

            return self::$_instance;
        }

        function checkCityNotExists($city, $cp) {
            // Fonction vérifiant si la ville existe déjà dans une base de données et retourne Vrai si la ville n'existe pas
            $requestCity = $this->bdd->prepare('SELECT count(*) FROM city WHERE city = :city AND cp = :cp;');
            $requestCity->bindParam(":city", $city, PDO::PARAM_STR);
            $requestCity->bindParam(":cp", $cp, PDO::PARAM_STR);

            $requestCity->execute();
            $nb = $requestCity->fetchColumn();

            if ($nb == 0) {
                return true;
            } else {
                return false;
            }
        }

        function InsertCity($city, $cp) {
            // Insère la ville
            $requestCity = $this->bdd->prepare('INSERT INTO city(city, cp) VALUES (:city, :cp);');
            $requestCity->bindValue(":city", htmlentities($city), PDO::PARAM_STR);
            $requestCity->bindValue(":cp", $cp, PDO::PARAM_STR);

            $requestCity->execute();
        }

        function GetCity($city, $cp) {
            // Récupère l'id de la ville correspondant à celle qui est insérée par le nouvel utilisateur
            $requestCity = $this->bdd->prepare('SELECT id_city FROM city WHERE city = :city AND cp = :cp;');
            $requestCity->bindValue(":city", $city, PDO::PARAM_STR);
            $requestCity->bindValue(":cp", $cp, PDO::PARAM_STR);

            $requestCity->execute();

            $id_city = $requestCity->fetchColumn();

            return $id_city;
        }

        function InsertUser($city, $cp, $gender,$email, $pwd, $address, $phone, $type) {
            // Insertion de la partie commune de l'utilisateur en vérifiant la ville et récupérant son id
            if ($this->checkCityNotExists($city, $cp)) {
                // Si la ville n'existe pas, on l'ajoute dans la base de données
                $this->InsertCity($city, $cp);
            }

            $id_city = $this->GetCity($city, $cp);

            $requestUser = $this->bdd->prepare('INSERT INTO user(id_city, gender, email, password, address, phone, type) VALUES (:id_city, :gender, :email, :pwd, :address, :phone, :type);');
            $requestUser->bindValue(':id_city', $id_city, PDO::PARAM_INT);
            $requestUser->bindValue(':gender', htmlentities($gender), PDO::PARAM_STR);
            $requestUser->bindValue(':email', htmlentities($email), PDO::PARAM_STR);
            $requestUser->bindValue(':pwd', htmlentities($pwd), PDO::PARAM_STR);
            $requestUser->bindValue(':address', htmlentities($address), PDO::PARAM_STR);
            $requestUser->bindValue(':phone', htmlentities($phone), PDO::PARAM_STR);
            $requestUser->bindValue(':type', $type, PDO::PARAM_STR);

            $success = $requestUser->execute();

            if (!$success) {
                echo "L'utlisateur n'a pas pu être inséré";
            }
        }

        function getNewIdUser() {
            $getId_user = $this->bdd->prepare("SELECT MAX(id_user) FROM user;");
            $getId_user->execute();

            return $getId_user->fetchColumn();
        }

        function InsertCompany ($company, $typeCompany, $lastname, $firstname) {
            // Insertion de la partie société
            $requestCompany = $this->bdd->prepare('INSERT INTO company(company, type_comp, lastname_contact, firstname_contact, id_user) VALUES (:company, :type_comp, :lastname, :firstname_contactCO, :id_u);');
            $requestCompany->bindValue(":id_u", $this->getNewIdUser(), PDO::PARAM_INT);
            $requestCompany->bindValue(":company", htmlentities($company), PDO::PARAM_STR);
            $requestCompany->bindValue(":type_comp", $typeCompany, PDO::PARAM_STR);
            $requestCompany->bindValue(":lastname", htmlentities($lastname), PDO::PARAM_STR);
            $requestCompany->bindValue(":firstname", htmlentities($firstname), PDO::PARAM_STR);

            if (!$requestCompany->execute()) {
                echo "L'inscription a été interrompue";
            } else {
                echo '<p>Vous avez été bien enregistré. Vous serez redirigé dans 5 secondes</p>';

                General::getInstance()->redirectWithRefresh('index', 5000);
            }


        }

        function InsertCustomer($lastname, $firstname) {
            // Insertion de la partie particulier
            $requestCustomer = $this->bdd->prepare('INSERT INTO customer(firstname, lastname, id_user) VALUES (:firstname, :lastname, :id_u);');
            $requestCustomer->bindValue(":lastname", $this->getNewIdUser(), PDO::PARAM_INT);
            $requestCustomer->bindValue(":lastname", htmlentities($lastname), PDO::PARAM_STR);
            $requestCustomer->bindValue(":firstname", htmlentities($firstname), PDO::PARAM_STR);

            if (!$requestCustomer->execute()) {
                echo "L'inscription a été interrompue";
            } else {
                echo '<p>Vous avez été bien enregistré. Vous serez redirigé dans 5 secondes</p>';

                General::getInstance()->redirectWithRefresh('index', 5000);
            }
        }
    }