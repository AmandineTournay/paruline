<?php
    include_once "General.php";

    class TailorMade {
        private static $_instance = null;
        private $bdd;

        private function __construct() {
            $this->bdd = General::getInstance()->connect("localhost", "paruline", "root", "");
        }

        public static function getInstance() {
            if (is_null(self::$_instance)) {
                self::$_instance = new TailorMade();
            }

            return self::$_instance;
        }

        public function getCatCurtains () {
            $categorie = [];

            $request = $this->bdd->prepare("SELECT id_rideaux, catTissu FROM rideaux;");
            $request->execute();

            while ($fetch = $request->fetch(PDO::FETCH_ASSOC)) {
                $line = new stdClass();

                $line->id_cat = $fetch["id_rideaux"];
                $line->catCur = $fetch["catTissu"];

                $categorie[] = $line;
            }
            unset($fetch);

            return $categorie;
        }

        public function getCurtains ($id_rideaux) {
            $tissus = array();

            $request = $this->bdd->prepare('SELECT id_typeRideaux, nom_t, prix_au_Metre, largeur FROM tissus WHERE id_rideaux = :id_rideaux;');
            $request->bindValue(':id_rideaux', $id_rideaux, PDO::PARAM_INT);

            $request->execute();

            while ($fetch = $request->fetch(PDO::FETCH_ASSOC)) {
                $tissus[] = [$fetch['id_typeRideaux'], $fetch['nom_t'], $fetch['prix_au_Metre'], $fetch['largeur']];
            }
            unset($fetch);

            return json_encode($tissus);
        }

        public function getCurConfection () {
            $finitions = array();

            $request = $this->bdd->prepare('SELECT id_typeCC, nom_typeC, prix_typeC FROM typeconfectionC;');
            $request->execute();

            while ($fetch = $request->fetch(PDO::FETCH_ASSOC)) {
                $line = new stdClass();

                $line->idConf = $fetch['id_typeCC'];
                $line->nomC = $fetch["nom_typeC"];

                $finitions[] = $line;
            }
            unset($fetch);

            return $finitions;
        }

        public function getCatSheers () {
            $categorie = [];

            $request = $this->bdd->prepare("SELECT id_voilages, catVoilage FROM voilage;");
            $request->execute();

            while ($fetch = $request->fetch(PDO::FETCH_ASSOC)) {
                $line = new stdClass();

                $line->id_cat = $fetch["id_voilages"];
                $line->catCur = $fetch["catVoilage"];

                $categorie[] = $line;
            }
            unset($fetch);

            return $categorie;
        }

        public function getSheers ($id_voilage) {
            $voilages = array();

            $request = $this->bdd->prepare('SELECT id_typeVoilages, nom_v, prix_metre FROM typeVoilage WHERE id_voilages = :id_voilages;');
            $request->bindValue(':id_voilages', $id_voilage, PDO::PARAM_INT);

            $request->execute();

            while ($fetch = $request->fetch(PDO::FETCH_ASSOC)) {
                $voilages[] = [$fetch['id_typeVoilages'], $fetch['nom_v'], $fetch['prix_metre']];
            }
            unset($fetch);

            return json_encode($voilages);
        }

        public function getSheersDetails ($id_voilage) {

            $request = $this->bdd->prepare('SELECT ourlet FROM typeVoilage WHERE id_typeVoilages = :idTypeVoilage;');
            $request->bindValue(':id_typeVoilage', $id_voilage, PDO::PARAM_INT);

            $request->execute();

            $fetch = $request->fetch(PDO::FETCH_ASSOC);
            $details = [$fetch['ourlet']];
            unset($fetch);

            return json_encode($details);
        }

        public function getSheeConfection () {
            $finitions = array();

            $request = $this->bdd->prepare('SELECT id_typeCS, nom_typeCS, prix_typeCS FROM typeconfectionS;');
            $request->execute();

            while ($fetch = $request->fetch(PDO::FETCH_ASSOC)) {
                $line = new stdClass();

                $line->idConf = $fetch['id_typeCS'];
                $line->nomC = $fetch["nom_typeCS"];

                $finitions[] = $line;
            }
            unset($fetch);

            return $finitions;
        }
    }