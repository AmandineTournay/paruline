<?php
    include_once "General.php";

    class Admin {
        private static $_instance = null;
        private $bdd;

        private function __construct() {
            $this->bdd = General::getInstance()->connect("localhost", "paruline", "root", "");
        }

        public static function getInstance() {
            if (is_null(self::$_instance)) {
                self::$_instance = new Admin();
            }

            return self::$_instance;
        }

        function getProviders() {
            $providers = array();

            $requestProf = $this->bdd->prepare("SELECT name_f, adresse_f FROM fournisseur;");
            $requestProf->execute();

            while ($fetch = $requestProf->fetch(PDO::FETCH_ASSOC)) {
                $line = new stdClass();

                $line->name = $fetch['name_f'];
                $line->address = $fetch['adresse_f'];

                $providers[] = $line;
            }
            unset($fetch);

            return $providers;
        }

        function getCustomersProf() {
            $professionnals = array();

            $requestProf = $this->bdd->prepare("SELECT lastname_contact, firstname_contact, company, type_comp, email, address, cp, city, phone
                                                FROM user U, company COMP, city CIT
                                                WHERE U.id_user = COMP.id_user AND U.id_city = CIT.id_city AND U.id_user > 1 AND type = 'company';");
            $requestProf->execute();

            while ($fetch = $requestProf->fetch(PDO::FETCH_ASSOC)) {
                $line = new stdClass();

                $line->company = $fetch['company'];
                $line->lastname = $fetch['lastname_contact'];
                $line->firstname = $fetch['firstname_contact'];
                $line->email = $fetch['email'];
                $line->phone = $fetch['phone'];
                $line->address = $fetch['address'];
                $line->cp = $fetch['cp'];
                $line->city = $fetch['city'];

                $professionnals[] = $line;
            }
            unset($fetch);

            return $professionnals;
        }

        function getCustomers() {
            $customers = array();

            $requestCustomers = $this->bdd->prepare("SELECT lastname, firstname, email, address, cp, city, phone
                                                    FROM user U, customer CUS, city CIT
                                                    WHERE U.id_user = CUS.id_user AND U.id_city = CIT.id_city AND U.id_user > 1 AND type = 'customer';");
            $requestCustomers->execute();

            while ($fetch = $requestCustomers->fetch(PDO::FETCH_ASSOC)) {
                $line = new stdClass();

                $line->lastname = $fetch['lastname'];
                $line->firstname = $fetch['firstname'];
                $line->email = $fetch['email'];
                $line->phone = $fetch['phone'];
                $line->address = $fetch['address'];
                $line->cp = $fetch['cp'];
                $line->city = $fetch['city'];

                $customers[] = $line;
            }
            unset($fetch);

            return $customers;
        }

        public function updateAccessory($id) {
            $title = $_POST['title'];
            $description = $_POST['description'];

            $updateAccessoryName = $this->bdd->prepare("UPDATE products P SET P.name_product = :name_p WHERE P.id_products = :id_p;");
            $updateAccessoryName->bindValue(":name_p", htmlentities($title), PDO::PARAM_STR);
            $updateAccessoryName->bindValue(":id_p", htmlentities($id), PDO::PARAM_INT);
            $updateAccessoryName->execute();

            $updateAccessory = $this->bdd->prepare("UPDATE accessories A SET A.description = :description  WHERE A.id_products = (SELECT P.id_products FROM products P WHERE P.id_products = :id_p);");
            $updateAccessory->bindValue(':description', htmlentities($description), PDO::PARAM_STR);
            $updateAccessory->bindValue(':id_p', $id, PDO::PARAM_INT);
            $updateAccessory->execute();
        }

        public function updatePriceAccess($id) {
            $priceF = $_POST['fournisseur'];
            $coeff = $_POST['coeff'];
            $ref = $_POST['ref'];

            $updateAccessory = $this->bdd->prepare("CALL UpdatePriceAcc(:id, :ref, :prF, :coeff)");
            $updateAccessory->bindValue(':id', $id, PDO::PARAM_INT);
            $updateAccessory->bindValue(':ref', htmlentities($ref), PDO::PARAM_STR);
            $updateAccessory->bindValue(':prF', $priceF, PDO::PARAM_STR);
            $updateAccessory->bindValue(':coeff', $coeff, PDO::PARAM_STR);

            $updateAccessory->execute();
        }
    }