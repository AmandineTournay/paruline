<?php
    include_once "General.php";

    class User {
        private static $_instance = null;
        private $bdd;

        private function __construct() {
            $this->bdd = General::getInstance()->connect("localhost", "paruline", "root", "");
        }

        public static function getInstance() {
            if (is_null(self::$_instance)) {
                self::$_instance = new User();
            }

            return self::$_instance;
        }

        function userInfos($id_user, $type) {
            $userInfos = array();

            if ($type == 'company') {
                $requestInfosCO = $this->bdd->prepare("SELECT u.id_user, email, password, address, cp, city, phone, company, type_comp, lastname_contact, firstname_contact
                                                      FROM user U, company COM, city CIT
                                                      WHERE COM.id_user = U.id_user AND U.id_city = CIT.id_city AND U.id_user = :id_user;");
                $requestInfosCO->bindValue(':id_user', $id_user, PDO::PARAM_INT);

                $requestInfosCO->execute();

                while ($fetchCO = $requestInfosCO->fetch(PDO::FETCH_ASSOC)) {
                    $line = new stdClass();

                    $line->id_user = $fetchCO['id_user'];
                    $line->lastname = $fetchCO['lastname_contact'];
                    $line->firstname = $fetchCO['firstname_contact'];
                    $line->email = $fetchCO['email'];
                    $line->password = $fetchCO['password'];
                    $line->address = $fetchCO['address'];
                    $line->cp = $fetchCO['cp'];
                    $line->city = $fetchCO['city'];
                    $line->phone = $fetchCO['phone'];
                    $line->company = $fetchCO['company'];
                    $line->type_comp = $fetchCO['type_comp'];

                    $userInfos[] = $line;
                }
                unset($fetchCO);
            }
            elseif ($type == 'customer') {
                $requestInfosCU = $this->bdd->prepare("SELECT U.id_user, email, address, cp, city, phone, lastname, firstname 
                                                      FROM user U, customer CUS, city CIT
                                                      WHERE U.id_user = CUS.id_user AND U.id_city = CIT.id_city AND U.id_user = :id_user;");
                $requestInfosCU->bindValue(':id_user', $id_user, PDO::PARAM_INT);

                $requestInfosCU->execute();

                while ($fetchCU = $requestInfosCU->fetch(PDO::FETCH_ASSOC)) {
                    $line = new stdClass();

                    $line->id_user = $fetchCU['id_user'];
                    $line->lastname = $fetchCU['lastname_contact'];
                    $line->firstname = $fetchCU['firstname_contact'];
                    $line->email = $fetchCU['email'];
                    $line->address = $fetchCU['address'];
                    $line->cp = $fetchCU['cp'];
                    $line->city = $fetchCU['city'];
                    $line->phone = $fetchCU['phone'];

                    $userInfos[] = $line;
                }
                unset($fetchCU);
            }

            return $userInfos;
        }

        function updateGeneral($id_user) {

            $lastname = strip_tags(trim($_POST['lastname']));
            $firstname = strip_tags(trim($_POST['firstname']));
            $address = strip_tags(trim($_POST['address']));
            $city = strip_tags(trim($_POST['city']));
            $cp = strip_tags(trim($_POST['cp']));
            $phone = strip_tags(trim($_POST['phone']));

            if ($_SESSION['type'] == 'company') {
                $updateCompany = $this->bdd->prepare("CALL updateGeneralCOMP(:lastname, :firstname, :address, :phone, :city, :cp, :id_user)");
                $updateCompany->bindValue(':lastname', htmlentities($lastname), PDO::PARAM_STR);
                $updateCompany->bindValue(':firstname', htmlentities($firstname), PDO::PARAM_STR);
                $updateCompany->bindValue(':address', htmlentities($address), PDO::PARAM_STR);
                $updateCompany->bindValue(':phone', htmlentities($phone), PDO::PARAM_STR);
                $updateCompany->bindValue(':city', htmlentities($city), PDO::PARAM_STR);
                $updateCompany->bindValue(':cp', $cp, PDO::PARAM_STR);
                $updateCompany->bindValue(':id_user', $id_user, PDO::PARAM_INT);

                if ($updateCompany->execute()) {
                    echo "<script>console.log('CALL fait');</script>";
                }
                else{
                    echo "<script>console.log('CALL fait pas');</script>";
                }
            }
            elseif ($_SESSION['type'] == 'customer') {
                $updateCustomer = $this->bdd->prepare("CALL updateGeneralCUST(:lastname, :firstname, :address, :phone, :city, :cp, :id_user)");
                $updateCustomer->bindValue(':lastname', htmlentities($lastname), PDO::PARAM_STR);
                $updateCustomer->bindValue(':firstname', htmlentities($firstname), PDO::PARAM_STR);
                $updateCustomer->bindValue(':address', htmlentities($address), PDO::PARAM_STR);
                $updateCustomer->bindValue(':phone', htmlentities($phone), PDO::PARAM_STR);
                $updateCustomer->bindValue(':city', htmlentities($city), PDO::PARAM_STR);
                $updateCustomer->bindValue(':cp', $cp, PDO::PARAM_STR);
                $updateCustomer->bindValue(':id_user', $id_user, PDO::PARAM_INT);

                $updateCustomer->execute();
            }
        }

        function updateCompany($id_user) {

            $company = strip_tags(trim($_POST['company']));
            $type_comp = strip_tags(trim($_POST['type_comp']));

            if ($_SESSION['type'] == 'company') {
                $updateCompany = $this->bdd->prepare("UPDATE user U, company COM
                                                    SET company = :company, type_comp = :type_comp
                                                    WHERE U.id_user = COM.id_user AND U.id_user = :id_user;");
                $updateCompany->bindValue(':company', htmlentities($company), PDO::PARAM_STR);
                $updateCompany->bindValue(':type_comp', $type_comp, PDO::PARAM_STR);
                $updateCompany->bindValue(':id_user', $id_user, PDO::PARAM_STR);

                $updateCompany->execute();
            }
        }

        function updateConnexion($id_user) {

            $email = strip_tags(trim($_POST['email']));
            //$oldPwd = strip_tags(trim($_POST['oldPwd']));
            $newPwd1 = strip_tags(trim($_POST['newPwd1']));
            $newPwd2 = strip_tags(trim($_POST['newPwd2']));

            if ($newPwd1 == $newPwd2) {
                $updateConnexion = $this->bdd->prepare("UPDATE user SET email = :email, password = :pwd WHERE id_user = :id_user;");
                $updateConnexion->bindValue(':email', htmlentities($email), PDO::PARAM_STR);
                $updateConnexion->bindValue(':pwd', htmlentities(sha1($newPwd1)), PDO::PARAM_STR);
                $updateConnexion->bindValue(':id_user', $id_user, PDO::PARAM_INT);

                $updateConnexion->execute();

                $_SESSION['email'] = $email;
            }
            else {
                echo 'Les mots de passe ne correspondent pas';
            }
        }
    }