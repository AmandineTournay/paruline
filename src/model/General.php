<?php

class General {
    private static $_instance = null;

    private function __construct() {

    }

    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new General();
        }

        return self::$_instance;
    }

    public function connect($host, $db, $user, $pwd) {
        try {
            return new PDO("mysql:host=$host;dbname=$db;charset=utf8", $user, $pwd);
        }
        catch (Exception $e) {
            return "Il y a un problème de connexion avec la base de données : " .$e->getMessage();
        }
    }

    public function redirectWithoutRefresh($page) {
        echo '<script> location.replace("'.$page.'.php"); </script>';
    }

    public function redirectWithRefresh($page, $countdownms) {
        echo "<script> setTimeout(function(){ location.replace('".$page.".php'); }, ".$countdownms."); </script>";
    }

    function autorisation() {
        if (!isset($_SESSION['connecte'])){
            header('Location:../../index.php');
        }
    }
}