<?php
    include_once "General.php";

    class Cart {
        private static $_instance = null;
        private $bdd;

        private function __construct() {
            $this->bdd = General::getInstance()->connect("localhost", "paruline", "root", "");
        }

        public static function getInstance() {
            if (is_null(self::$_instance)) {
                self::$_instance = new Cart();
            }

            return self::$_instance;
        }

        private function getProductData($id) {

            $statement = $this->bdd->prepare("SELECT name_product, prTotalAccess FROM products P, accessories A, priceaccesso Pr
                                            WHERE P.id_products = A.id_products AND Pr.id_products = P.id_products AND P.id_products = :id_products LIMIT 1;");
            $statement->bindParam(':id_products', $id);
            $statement->execute();

            while ($fetch = $statement->fetch(PDO::FETCH_ASSOC)) {
                $line = new stdClass();

                $line->product = $fetch['name_product'];
                $line->price = $fetch['prTotalAccess'];
            }
            unset($fetch);

            return $line;
        }

        public function getProducts() {
            $array = array();

            $statement = $this->bdd->prepare("SELECT P.id_products, A.id_products, name_product, prTotalAccess, description FROM products P, accessories A, priceaccesso Pr
                                            WHERE P.id_products = A.id_products AND Pr.id_products = P.id_products ORDER BY name_product ASC;");
            $statement->execute();

            while ($fetch = $statement->fetch(PDO::FETCH_ASSOC)) {
                $line = new stdClass();

                $line->id_product = $fetch['id_products'];
                $line->product = $fetch['name_product'];
                $line->description = $fetch['description'];
                $line->price = $fetch['prTotalAccess'];

                $array[] = $line;
            }
            unset($fetch);

            return $array;
        }

        public function getCart(){
            // Fonction permettant de récupérer les données du panier provenant de $_SESSION['cart']
            $cartArray = array();

            if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){
                $cart = json_decode($_SESSION['cart'], true);

                for($i = 0; $i < count($cart); $i++){
                    $lines = $this->getProductData($cart[$i]["id_product"]);

                    $line = new stdClass();

                    $line->id_product = $cart[$i]["id_product"];
                    $line->product = $lines->product;
                    $line->quantity = $cart[$i]["quantity"];
                    $line->price = $lines->price;
                    $line->totalProduct = ($lines->price*$cart[$i]["quantity"]);

                    $cartArray[] = $line;
                }
            }
            return $cartArray;
        }

        public function getPriceAccesso() {
            $array = array();

            $statement = $this->bdd->prepare("SELECT P.id_products, name_product, ref, name_f, prFournisseur, prPondere, coeff, margeMonetaire, prTotalAccess
                                            FROM products P, fournisseur F, priceaccesso Pr
                                            WHERE P.id_products = Pr.id_products AND P.id_fourniss = F.id_fourniss;");
            $statement->execute();

            while ($fetch = $statement->fetch(PDO::FETCH_ASSOC)) {
                $line = new stdClass();

                $line->id_products = $fetch["id_products"];
                $line->nameA = $fetch["name_product"];
                $line->ref = $fetch["ref"];
                $line->nameF = $fetch["name_f"];
                $line->prFournisseur = $fetch["prFournisseur"];
                $line->prPondere = $fetch["prPondere"];
                $line->coeff = $fetch["coeff"];
                $line->margeMonetaire = $fetch["margeMonetaire"];
                $line->prTotalAccess = $fetch["prTotalAccess"];

                $array[] = $line;
            }
            unset($fetch);

            return $array;
        }


        public function addToCart($quantity, $id) {
            // Fonction permettant d'ajouter un produit au panier

            // On vérifie si l'id est valide puis s'il n'existe pas dans $_SESSION['cart'], on l'ajoute.
            // S'il existe déjà, on fait une boucle pour voir si on trouve le produit puis on incrémente la quantité.
            // Si on ne trouve pas le produit, on le rajoute avec une quantité valant 1 et on ajoute le tout dans $_SESSION['cart'].
            // Cela permet d'éviter des doublons dans $_SESSION['cart'].

            if ($id > 0) {
                if (isset($_SESSION['cart'])) {
                    $cart = json_decode($_SESSION['cart'], true);
                    $found = false;

                    for ($i = 0; $i < count($cart); $i++) {
                        if ($cart[$i]['id_product'] == $id) {
                            $cart[$i]['quantity'] = $cart[$i]['quantity'] + $quantity;

                            $found = true;
                            break;
                        }
                    }

                    if (!$found) {
                        $line = new stdClass();

                        $line->id_product = $id;
                        $line->quantity = $quantity;

                        $cart[] = $line;
                    }

                    $_SESSION['cart'] = json_encode($cart);
                }
                else {
                    $line = new  stdClass();

                    $line->id_product = $id;
                    $line->quantity = $quantity;

                    $cart[] = $line;

                    $_SESSION['cart'] = json_encode($cart);
                }
            }
        }

        public function updateFromCart($id, $quantity) {
            // Fonction permettant de modifier la quantité depuis le panier

            if ($id > 0) {
                if (isset($_SESSION['cart'])) {
                    $cart = json_decode($_SESSION['cart'], true);

                    for ($i = 0; $i < count($cart); $i++) {
                        if ($cart[$i]['id_product'] == $id) {
                            $cart[$i]['quantity'] = $quantity;

                            if ($quantity < 1) {
                                unset($cart[$i]);
                            }
                            break;
                        }
                    }

                    // Permet de réarranger le panier après la modification d'un élément
                    $cart = array_values($cart);
                    $_SESSION['cart'] = json_encode($cart);
                }
            }
        }

        public function removeFromCart($id) {
            // Fonction permettant de supprimer un produit du panier

            if ($id > 0) {
                if (isset($_SESSION['cart'])) {
                    $cart = json_decode($_SESSION['cart'], true);

                    for ($i = 0; $i < count($cart); $i++) {
                        if ($cart[$i]['id_product'] == $id) {
                            unset($cart[$i]);

                            // Permet de réarranger le panier après la suppression d'un élément
                            $cart = array_values($cart);
                            $_SESSION['cart'] = json_encode($cart);
                        }
                    }
                }
            }
        }

        public function emptyCart() {
            // Fonction permettant de vider entièrement le panier
            $_SESSION['cart'] = "";
        }

        public function totalCart() {
            // Fonction permettant de calculer le prix total de la commande
            $totalCart = array();
            $cart = $this->getCart();

            if (isset($_SESSION['cart'])) {
                $total = 0;

                // Calcul du prix total
                foreach ($cart as $product) {
                    $total = $total + $product->totalProduct;
                }

                $line = new stdClass();

                $line->totalCart = $total;

                $totalCart[] = $line;
            }
            return $totalCart;
        }

    }