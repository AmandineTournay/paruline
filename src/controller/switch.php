<?php
    session_start();
    include_once "../model/General.php";
    include_once "../model/Cart.php";
    include_once "../model/User.php";
    include_once "../model/TailorMade.php";

    General::getInstance()->connect("localhost", "paruline", "root", "");

    if (isset($_POST['id'])){
        $id = strip_tags(trim(intval($_POST['id'])));
    }
    if (isset($_POST['action'])) {
        $action = strip_tags(trim($_POST['action']));
    }
    if (isset($_POST['quantity'])) {
        $quantity = strip_tags(trim(intval($_POST['quantity'])));
    }

    switch ($action) {

        // SUR MESURE
        case 'curtains':
            print TailorMade::getInstance()->getCurtains($id);
            break;

        case 'sheers':
            print TailorMade::getInstance()->getSheers($id);
            break;

        case 'sheersDetails' :
            print TailorMade::getInstance()->getSheersDetails($id);
            break;

        // PANIER
        case 'add':
            Cart::getInstance()->addToCart($quantity, $id);
            break;

        case 'update':
            Cart::getInstance()->updateFromCart($id, $quantity);
            break;

        case 'remove':
            Cart::getInstance()->removeFromCart($id);
            break;

        case 'empty':
            Cart::getInstance()->emptyCart();
            break;

        // MISE A JOUR DES INFORMATIONS UTILISATEUR
        case 'updateGeneral':
            User::getInstance()->updateGeneral($id);
            break;

        case 'updateCompany':
            User::getInstance()->updateCompany($id);
            break;

        case 'updateConnexion':
            User::getInstance()->updateConnexion($id);
            break;
    }