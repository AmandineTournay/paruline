<?php
    include_once "model/General.php";
    $bdd = General::getInstance()->connect("localhost", "paruline", "root", "");

    if (isset($_POST['submitLogin'])) {
        if (empty(strip_tags(trim($_POST['email']))) && empty(strip_tags(trim($_POST['password'])))) {
            echo "<p class='textAlign'>Veuillez renseigner l'adresse email et le mot de passe</p>";
        }
        else {
            if (empty(strip_tags(trim($_POST['email'])))) {
                echo "<p class='textAlign'>Veuillez renseigner l'adresse email</p>";
            }
            else {
                if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                    echo "<p class='textAlign'>Adresse e-mail non autorisée</p>";
                }
                else {
                    $email = strip_tags(trim($_POST['email']));

                    if (empty(strip_tags(trim($_POST['password'])))) {
                        echo '<p class="textAlign">Veuillez renseigner le mot de passe</p>';
                    }
                    else {
                        // ^(début), $(fin), (?=.*\d)(au moins 1 chiffre), (?=.*[A-Za-z])(au moins 1 lettre), {8-12}(entre 8 et 12 caractères) et !@#$% -> [0-9A-Za-z!@#$%]
                        //(ça doit être un nombre, une lettre ou un caractère spécial suivant)

                        if (!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,30}$/', $_POST['password'])) {
                            echo "<p>Le mot de passe ne correspond pas aux critères</p>";
                        }
                        else {
                            $pwd = sha1($_POST['password']);

                            $login = $bdd->prepare("SELECT id_user, email, password, type FROM user WHERE email = :email AND password = :pwd;");
                            $login->bindParam(':email', $email, PDO::PARAM_STR);
                            $login->bindParam(':pwd', $pwd, PDO::PARAM_STR);

                            if ($login->execute()) {

                                if ($fetch = $login->fetch()) {
                                    $_SESSION['type'] = $fetch['type'];
                                    $_SESSION['email'] = $fetch['email'];
                                    $_SESSION['id_user'] = $fetch['id_user'];
                                    $_SESSION['session'] = true;

                                    General::getInstance()->redirectWithoutRefresh('index');
                                }
                                else {
                                    echo '<p class="textAlign">Le compte est inexistant</p>';
                                }

                                unset($fetch);
                            }
                            else {
                                echo '<p class="textAlign">Il y a eu un problème lors de la requête</p>';
                            }
                        }
                    }
                }
            }
        }
    }