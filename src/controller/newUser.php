<?php
    include_once 'model/NewUser.php';

    if (isset($_POST['submitSignUp'])) {

        $type = strip_tags(trim($_POST['customerType']));
        $gender = strip_tags(trim($_POST['gender']));

        $lastname = strip_tags(trim($_POST['lastname']));
        $firstname = strip_tags(trim($_POST['firstname']));
        $company = strip_tags(trim($_POST['company']));
        $typeCompany = strip_tags(trim($_POST['typeCompany']));

        $city = strip_tags(trim($_POST['city']));
        $cp = strip_tags(trim($_POST['cp']));

        $address = strip_tags(trim($_POST['address']));
        $email = strip_tags(trim($_POST['email']));
        $pwd = strip_tags(trim($_POST['password']));
        $pwd2 = strip_tags(trim($_POST['password2']));
        $phone = strip_tags(trim($_POST['phone']));

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {

            echo "<p>Adresse e-mail non autorisée</p><br>";

        } else {

            // ^(début), $(fin), (?=.*\d)(au moins 1 chiffre), (?=.*[A-Za-z])(au moins 1 lettre), {8-30}(entre 8 et 30 caractères) et !@#$% -> [0-9A-Za-z!@#$%]
            //(ça doit être un nombre, une lettre ou un caractère spécial suivant)
            if (!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,30}$/', $pwd, $pwd2)) {

                echo "<p>Le mot de passe ne correspond pas aux critères</p><br>";

            } else {

                if (strip_tags(trim(sha1($_POST['password']))) == strip_tags(trim(sha1($_POST['password2'])))) {

                    NewUser::getInstance()->InsertUser($city, $cp, $gender, $email, sha1($pwd), $address, $phone, $type);

                    if ($_POST['customerType'] == 'company') {

                        NewUser::getInstance()->InsertCompany($company, $typeCompany, $lastname, $firstname);

                    }
                    elseif ($_POST['customerType'] == 'customer') {

                        NewUser::getInstance()->InsertCustomer($lastname, $firstname);
                    }
                } else {

                    echo "<p>Les mots de passe entrés ne correspondent pas</p><br>";

                }
            }
        }
    }