<?php
    include_once "model/Admin.php";
    include_once "model/Cart.php";
    include_once "model/User.php";
    include_once "model/TailorMade.php";

    $menu = isset($_GET['menu']) ? $_GET['menu'] : '';

    switch ($menu) {
        default:
            include_once 'view/404.php';
            break;

        case "":
            include_once 'view/buyMenu/buyHome.php';
            break;

        case 'userInfos':
            include_once 'view/buyMenu/userInfos.php';
            break;

        case 'providers':
            include_once 'view/buyMenu/providers.php';
            break;

        case 'customers':
            include_once 'view/buyMenu/customersList.php';
            break;

        case "accessPrices":
            include_once 'view/buyMenu/accessPrices.php';
            break;

        case 'cart':
            include_once 'view/buyMenu/cart.php';
            break;

        case "orderList":
            include_once 'view/buyMenu/orderList.php';
            break;

        case 'buyAccessories':
            include_once 'view/buyMenu/accessories.php';
            break;

        case 'tailor-made' :
            include_once 'view/buyMenu/tailor-made.php';
            break;
    }