<?php
    $prices = Cart::getInstance()->getPriceAccesso();
    $i = 1;

    // TODO: Produit fraîchement ajouté qui s'affiche automatiquement pour mettre son prix et pouvoir supprimer le produit
?>
    <h1 class="center-align userSelect">Liste des prix des accessoires</h1>

    <br><br>

    <table class="highlight centered responsive-table">
        <thead>
            <tr class="userSelect">
                <th>Nom</th>
                <th>Référence du produit</th>
                <th>Nom Fournisseur</th>
                <th>Prix Fournisseur</th>
                <th>Prix Pondéré</th>
                <th>Coefficient</th>
                <th>Marge Monétaire</th>
                <th>Prix total HT</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach ($prices as $price) :
                    ?>
                    <tr class="tableStriped">
                        <td style="padding: .5em"><?= utf8_encode($price->nameA);?></td>
                        <td>
                            <span id="ref<?= $i; ?>">
                                <?php
                                    if (empty($price->ref)) {
                                        echo "<b class='red-text userSelect'>AUCUNE RÉFÉRENCE !</b>";
                                    }
                                    else {
                                        print $price->ref;
                                    }
                                ?>
                            </span>
                        </td>
                        <td><span id="ref<?= $i; ?>"><?php print $price->nameF;?></span></td>
                        <td><span id="prFourn<?= $i; ?>"><?= $price->prFournisseur;?></span> €</td>
                        <td><span id="prPon<?= $i; ?>"><?= $price->prPondere;?></span> €</td>
                        <td><span id="coeff<?= $i; ?>"><?= $price->coeff;?></span> %</td>
                        <td><span id="marge<?= $i; ?>"><?= $price->margeMonetaire;?></span> €</td>
                        <td><span id="prTotal<?= $i; ?>"><?= $price->prTotalAccess;?></span> €</td>
                        <td>
                            <input type="hidden" id="idPrice<?= $i; ?>" value="<?= $price->id_products; ?>">
                            <button
                                    data-idprice="#idPrice<?= $i; ?>" data-ref="#ref<?= $i; ?>" data-fournis="#prFourn<?= $i; ?>" data-coeff="#coeff<?= $i; ?>"
                                    class="btn waves-effect waves-light updatePriceAccess">Modifier</button>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
            ?>
        </tbody>
    </table>