<?php
    include_once "model/Admin.php";
    $companies = Admin::getInstance()->getProviders();
?>

<h1 class="center-align userSelect">Liste des fournisseurs</h1>

<br><br><br>

<table class="highlight centered responsive-table">
    <thead>
        <tr class="userSelect">
            <th>Nom</th>
            <th>Adresse</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if ($companies == null) {
            echo "<p class='center-align userSelect'>Il n'y a pas des fournisseurs</p> <br><br><br>";
        }
        else {
            foreach ($companies as $company) {
                ?>
                <tr class="tableStriped">
                    <td><?php print $company->name; ?></td>
                    <td><?php print $company->address; ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>