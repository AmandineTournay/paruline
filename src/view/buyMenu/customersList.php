<?php
    $companies = Admin::getInstance()->getCustomersProf();
    $customers = Admin::getInstance()->getCustomers();
?>

    <h1 class="center-align userSelect">Liste des clients professionnels</h1>

    <br><br><br>

    <table class="highlight centered responsive-table">
        <thead>
            <tr class="userSelect">
                <th>Raison sociale</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>E-mail</th>
                <th>Téléphone</th>
                <th>Adresse</th>
                <th>Code Postal</th>
                <th>Ville</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if ($companies == null) {
                    echo "<p class='center-align userSelect'>Il n'y a pas de clients professionnels</p> <br><br><br>";
                }
                else {
                    foreach ($companies as $company) :
                        ?>

                            <tr class="tableStriped">
                                <td><?= print $company->company; ?></td>
                                <td><?= print $company->lastname; ?></td>
                                <td><?= print $company->firstname; ?></td>
                                <td><?= print $company->email; ?></td>
                                <td><?= print $company->phone ?></td>
                                <td><?= print $company->address ?></td>
                                <td><?= print $company->cp ?></td>
                                <td><?= print $company->city ?></td>
                            </tr>
                        <?php
                    endforeach;
                }
            ?>
        </tbody>
    </table>

    <br><br><br><br><br>

    <h1 class="center-align userSelect">Liste des clients particuliers</h1>

    <br><br><br>

    <table class="striped">
        <thead>
            <tr class="highlight centered responsive-table">
                <th>Nom</th>
                <th>Prénom</th>
                <th>E-mail</th>
                <th>Téléphone</th>
                <th>Adresse</th>
                <th>Code Postal</th>
                <th>Ville</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if ($customers == null) {
                    echo "<p class='center-align userSelect'>Il n'y a pas de clients patriculiers</p> <br><br><br>";
                }
                else {
                    foreach ($customers as $customer) :
                        ?>
                    <tr class="tableStriped">
                        <td><?= print $customer->lastname; ?></td>
                        <td><?= print $customer->firstname; ?></td>
                        <td><?= print $customer->email; ?></td>
                        <td><?= print $customer->phone; ?></td>
                        <td><?= print $customer->address; ?></td>
                        <td><?= print $customer->cp; ?></td>
                        <td><?= print $customer->city; ?></td>
                    </tr>
                        <?php
                    endforeach;
                }
            ?>
        </tbody>
    </table>