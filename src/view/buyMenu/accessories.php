<?php
    require_once "model/Cart.php";
    $products = Cart::getInstance()->getProducts();
    $i = 1;
    ?>

    <?php
    foreach ($products as $product) :
        ?>
        <div class="col s12 m4">
            <div class="card hoverable accessoCard centerAuto">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" src="../public/images/curtains.png" alt="">
                </div>

                <div class="card-content">
                    <p class="card-title activator grey-text text-darken-4"><span id="titleA<?= $i; ?>" class="activator"><?= $product->product; ?></span><i class="material-icons right">more_vert</i></p>
                    <div class="row">
                        <p class="truncate"><?= $product->description; ?></p>
                    </div>
                    <div class="row center-align priceAcc">
                        <h6><?= $product->price; ?> €</h6>
                    </div>
                </div>

                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i></span>
                    <div class="col s12">
                        <p><span id="descriptionA<?= $i; ?>"><?= $product->description; ?></span></p>
                    </div>
                </div>

                <div class="card-action center-align">
                    <?php
                        if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == 1) :
                            ?>
                            <input type="hidden" id="id_products<?= $i; ?>" value="<?= $product->id_product; ?>">
                            <button class="btn waves-effect waves-light updateAccess updateAccessories"
                                    data-idp="#id_products<?= $i; ?>" data-titleID="#titleA<?= $i; ?>" data-descriptionID="#descriptionA<?= $i; ?>"
                            >Modifier</button>
                            <?php
                        else :
                            ?>
                            <div class="input-field inline left quantityAcc">
                                <i class="material-icons prefix">add_shopping_cart</i>
                                <label for="quantity<?= $i; ?>">Quantité</label>
                                <input type="number" name="quantity" class="validate center-align no-spin" id="quantity<?= $i; ?>" min="1" value="1">
                                <span class="helper-text" data-error="Veuillez entrer un nombre entier" data-success=""></span>
                            </div>

                            <button class="btn waves-effect waves-light right addToCart"
                                    quantityID="#quantity<?= $i; ?>"
                                    data-id="<?php print $product->id_product; ?>"> Ajouter
                            </button>
                        <?php endif; ?>
                </div>
            </div>
        </div>
        <?php
        $i++;
    endforeach;
        ?>