<?php
    $categoriesCur = TailorMade::getInstance()->getCatCurtains();
    $confectionsC = TailorMade::getInstance()->getCurConfection();

    $categorieShee = TailorMade::getInstance()->getCatSheers();
    $confectionsS = TailorMade::getInstance()->getSheeConfection();


    $i = 1;
?>

    <div class="row flexWrap">
        <div class="col s12 m6 tailorBloc">
            <ul class="tabs center-align">
                <li class="tab heigthAuto"><a href="#curtainsTab">Rideaux</a></li>
                <li class="tab heigthAuto"><a href="#sheersTab">Voilages</a></li>
            </ul>

            <div id="curtainsTab">
                <?php include 'view/buyMenu/tailorMade/tailorCur.php'; ?>
            </div>

            <div id="sheersTab">
                <?php include 'view/buyMenu/tailorMade/tailorSheers.php'; ?>
            </div>
        </div>


        <div class="col s12 m6 informationBloc">

        </div>
    </div>