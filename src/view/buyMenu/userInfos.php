<?php
    $title = "Informations";
    require_once "model/User.php";

    $infos = User::getInstance()->userInfos($_SESSION['id_user'], $_SESSION['type']);

    foreach ($infos as $info) :
        ?>
        <div class="row center-align">
            <div class="col s12">
                <h2 class="userSelect">Informations de l'utilisateur</h2>
            </div>
        </div>

        <div class="row center-align">
            <div class="col s12">
                <h3 class="userSelect">Informations de connexion</h3>
            </div>
            <div class="col s12">
                <p><b class="userSelect">E-mail : </b><span class="infConnexion" data-editable><?= $info->email; ?></span></p>
            </div>
            <div class="col s12">
                <button class="btn waves-effect waves-light" id="updateConnexion">Modifier et/ou changer le mot de passe</button>
            </div>
        </div>

        <div class="row">
            <div class="col s12 <?= isset($_SESSION['type']) == 'company' ? 'm6' : ''; ?> center-align">
                <div class="col s12">
                    <h3 class="userSelect">Informations générales</h3>
                </div>

                <div class="row">
                    <div class="col s12 m6">
                        <p class="inline"><b class="userSelect">Nom :</b></p> <span class="infGeneral" data-editable><?= $info->lastname; ?></span>
                    </div>
                    <div class="col s12 m6">
                        <p class="inline"><b class="userSelect">Prénom : </b></p> <span class="infGeneral" data-editable><?= $info->firstname; ?></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12">
                        <p class="inline"><b class="userSelect">Adresse : </b></p> <span class="infGeneral" data-editable><?= $info->address; ?></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12 m6">
                        <p class="inline"><b class="userSelect">Ville : </b></p> <span class="infGeneral" data-editable><?= $info->city; ?></span>
                    </div>
                    <div class="col s12 m6">
                        <p class="inline"><b class="userSelect">Code Postal : </b></p> <span class="infGeneral" data-editable><?= $info->cp; ?></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12">
                        <p class="inline"><b class="userSelect">Téléphone : </b></p> <span class="infGeneral" data-editable><?= $info->phone; ?></span>
                    </div>
                </div>

                <div class="col s12">
                    <button class="btn waves-effect waves-light" id="updateGeneral">Modifier</button>
                </div>
            </div>

            <?php
                if (isset($_SESSION['type']) == 'company') :
                    ?>
                    <div class="col s12 m6 center-align">
                        <div class="col s12">
                            <h3 class="userSelect">Informations de l'entreprise</h3>
                        </div>

                        <div class="row">
                            <div class="col s12 m6">
                                <p class="inline"><b class="userSelect">Raison sociale : </b></p> <span class="infCompany" data-editable><?= utf8_encode($info->company); ?></span>
                            </div>
                            <div class="col s12 m6">
                                <p class="inline"><b class="userSelect">Type de la société : </b></p> <span class="infCompany" data-editable><?= utf8_encode($info->type_comp); ?></span>
                            </div>

                        </div>

                        <div class="col s12">
                            <button class="btn waves-effect waves-light" id="updateCompany">Modifier</button>
                        </div>
                    </div>
                <?php
                endif;
            ?>
        </div>

        <script>const dataId = "<?= isset($_SESSION['id_user']) ? $info->id_user : 'null'; ?>";</script>
    <?php
        endforeach;
        ?>
