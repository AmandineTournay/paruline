<br><br>

<form method="post">
    <div class="row">
        <div class="row"></div>

        <div class="row">
            <p class="col s12 m3">Choix du type de voilages : </p>

            <div class="col s12 m9">
            <?php
                foreach ($categorieShee as $categorieS) :
                    ?>
                    <input type="radio" id="fabricChoice<?php print $i; ?>" class="catVoilage radioStyle" name="fabricChoice" value="<?php print $categorieS->id_cat; ?>">
                    <label for="fabricChoice<?php print $i; ?>" class="labelRadio"><?php print $categorieS->catCur; ?></label>
                    <?php
                    $i++;
                endforeach;
            ?>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m5 push-m3 input-field">
                <select name="curtainsChoice" class="icons" id="selectVoilage">
                </select>
                <label>Sélectionner le voilage</label>
            </div>
        </div>

        <div class="row">
            <p class="col s12 m3">Choix de la finition : </p>

            <div class="col s12 m9">
            <?php
                foreach ($confectionsS as $confectionS) :
                    ?>
                    <input type="radio" id="confection<?php print $i; ?>" class="radioStyle" name="confection" value="<?php print $confectionS->idConf; ?>">
                    <label for="confection<?php print $i; ?>" class="labelRadio"><?php print $confectionS->nomC; ?></label>
                    <?php
                    $i++;
                endforeach;
            ?>
            </div>
        </div>

        <div id="row hiddenOurlet"></div>

        <div class="row TDvalign">
            <p class="col s12 m2">Taille du voilage :</p>

            <div class="col s2 m4 input-field inline">
                <input type="number" name="largeur" class="validate no-spin center-align" id="larg" min="0" step="0.1">
                <label for="larg">Largeur</label>
                <span class="helper-text" data-error="Entrez un nombre" data-success=""></span>
            </div>

            <p class="col s12 m1">X</p>

            <div class="col s12 m4 input-field inline">
                <input type="number" name="longueur" class="validate no-spin center-align" id="long" min="0" step="0.1">
                <label for="long">Longueur</label>
                <span class="helper-text" data-error="Entrez un nombre" data-success=""></span>
            </div>

            <i class="col s12 m1 material-icons help">help</i>
        </div>

        <div class="row">
            <p class="col s12 m4">Voilages en paires ou en panneaux : </p>

            <div class="col s12 m4">
                <input type="radio" id="paire" class="radioStyle" name="confection" value="paire">
                <label for="paire" class="labelRadio">Par Paire</label>
            </div>

            <div class="col s12 m4">
                <input type="radio" id="panneau" class="radioStyle" name="confection" value="panneau">
                <label for="panneau" class="labelRadio">Par Panneau</label>
            </div>
        </div>

        <div class="row">
            <p class="col s12 m2">Choix de l'ampleur : </p>

            <div class="col s12 m2">
                <select  name="amplitude">
                    <option value="1">1 cm</option>
                    <option value="1.5">1,5 cm</option>
                    <option value="2" selected>2 cm</option>
                    <option value="2.5">2,5 cm</option>
                    <option value="3">3 cm</option>
                </select>

            </div>

            <i class="col s12 m1 material-icons help">help</i>
        </div>

        <div class="row">
            <p class="col s12 m2 push-m4 TDvalign">Quantité : </p>

            <div class="col s12 m2 push-m4 input-field inline">
                <input type="number" name="quantity" class="validate no-spin center-align" id="quantityS" min="1" step="0.1" value="1">
                <label for="quantityS">Quantité</label>
                <span class="helper-text" data-error="Entrez un nombre" data-success=""></span>
            </div>
        </div>

        <div class="row">
            <button class="col s12 m3 push-m4 btn waves-effect waves-light">Commander</button>
        </div>
    </div>
</form>