<?php
    // Affichage du panier
    $products = Cart::getInstance()->getCart();
    $i = 1;

    // TODO: Mettre en forme correctement en ajoutant le total en bas à droite
    ?>
<div style="min-height: 615px">
    <table class="highlight centered responsive-table" id="cart">
        <thead>
            <tr class="userSelect">
                <th>Nom du produit</th>
                <th>Quantité</th>
                <th>Prix à l'unité HT</th>
                <th>Prix HT</th>
                <th id="changeCart"></th>
            </tr>
        </thead>

        <tbody>
            <?php
                foreach ($products as $product) :
                    ?>
                    <tr class="tableStriped">
                        <td><?= $product->product; ?></td>
                        <td class="updateQuantity" id="updateQuantity<?= $i; ?>" contenteditable="true"><?= $product->quantity; ?></td>
                        <td><?= $product->price; ?></td>
                        <td><?= $product->totalProduct; ?></td>
                        <td>
                            <button class="btn btn-flat waves-effect waves-orange orange-text updateCart" data-id="<?= $product->id_product; ?>">Mettre à jour</button>

                            <button class="btn btn-flat waves-effect waves-red red-text removeFromCart" updateQuanID="#updateQuantity<?= $i; ?>" data-id="<?= $product->id_product; ?>">Supprimer</button>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
            ?>
        </tbody>
    </table>

    <?php
        if (isset($_SESSION['cart'])):
            ?>
            <table class="responsive-table centered tableTotal">
                <thead>
                <tr class="userSelect">
                    <th>Total HT</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?php print $cart->totalCart() . " €"; ?></td>
                </tr>
                </tbody>
            </table>
            <?php
        endif;

        if (!isset($_SESSION['cart']) || empty($_SESSION['cart'])) :
            ?>
            <br><br><br><br><br>
            <p class="center userSelect">LE PANIER EST VIDE</p>
            <br><br><br>
    <?php
        endif;
    ?>
    <br><br>

    <div class="center">
        <button class="btn btn-flat waves-effect waves-ripple emptyCart">Vider le panier</button>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="index.php?p=checkout" class="btn waves-effect waves-ripple">Passer la commande</a>
    </div>
</div>