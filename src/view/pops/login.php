<div id="login" class="modal modal-fixed-footer modalLogin">
    <form method="post">
        <div class="modal-content">
            <h4 class="userSelect">Connexion</h4>

            <div class="container">
                <div class="row">
                    <div class="col s12 center-align">
                        <?php include_once "controller/login.php"; ?>
                    </div>

                    <div class="col s12 m6">
                        <div class="input-field inline">
                            <i class="material-icons prefix userSelect">email</i>
                            <input type="email" name="email" class="validate" id="email_log">
                            <label for="email_log" class="userSelect">E-mail</label>
                        </div>
                    </div>

                    <div class="col s12 m6">
                        <div class="input-field inline">
                            <i class="material-icons prefix userSelect">enhanced_encryption</i>
                            <input type="password" name="password" class="validate" id="pwd_log">
                            <label for="pwd_log" class="userSelect">Mot de passe</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <a class="btn-flat modal-action modal-close waves-effect waves-red">Annuler</a>
            <button type="submit" name="submitLogin" class="btn modal-action waves-effect waves-green">Se connecter</button>
        </div>
    </form>
</div>