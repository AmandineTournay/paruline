
<div id="signUp" class="modal modal-fixed-footer modalSignUp userSelect">
    <form method="post">
        <div class="modal-content">
            <div class="row">
                <h4 class="col s12">S'inscrire</h4>
                <p class="col s12 center-align"><?php include_once "controller/newUser.php"; ?></p>

                <div class="col s12 m6 center-align hiddendiv">
                    <label>
                        <input type="radio" name="customerType" value="customer">
                        <span>Particulier</span>
                    </label>
                </div>
                <div class="col s12 m6 center-align hiddendiv">
                    <label>
                        <input type="radio" name="customerType" value="company" checked>
                        <span>Professionnel</span>
                    </label>
                </div>

                <div class="col s12 m6 center-align">
                    <label>
                        <input type="radio" name="gender" value="M.">
                        <span>Monsieur</span>
                    </label>
                </div>
                <div class="col s12 m6 center-align">
                    <label>
                        <input type="radio" name="gender" value="Mme.">
                        <span>Madame</span>
                    </label>
                </div>

                <div class="col s12">

                </div>

                <div class="col s12 m6 center-align">
                    <div class="input-field inline width25">
                        <i class="material-icons prefix">person</i>
                        <input type="text" name="lastname" id="lastname_sign">
                        <label for="lastname_sign">Nom</label>
                    </div>
                </div>
                <div class="col s12 m6 center-align">
                    <div class="input-field inline width25">
                        <input type="text" name="firstname" id="firstname_sign">
                        <label for="firstname_sign">Prénom</label>
                    </div>
                </div>

                <div class="col s12 m6 center-align">
                    <div class="input-field inline">
                        <i class="material-icons prefix">domain</i>
                        <input type="text" name="company" id="company_sign">
                        <label for="company_sign">Raison sociale</label>
                    </div>
                </div>
                <div class="col s12 m6 center-align">
                    <div class="input-field inline">
                        <input type="text" name="typeCompany" id="companyType_sign">
                        <label for="companyType_sign">Forme juridique</label>
                    </div>
                </div>

                <div class="col s12 center-align">
                    <div class="input-field inline addressCity">
                        <i class="material-icons prefix">location_city</i>
                        <input type="text" name="address" id="address_sign">
                        <label for="address_sign">Adresse</label>
                    </div>
                </div>

                <div class="col s12 m6 center-align">
                    <div class="input-field inline">
                        <i class="material-icons prefix">location_city</i>
                        <input type="text" name="city" id="city_sign">
                        <label for="city_sign">Ville</label>
                    </div>
                </div>
                <div class="col s12 m6 center-align">
                    <div class="input-field inline">
                        <input type="number" name="cp" class="validate no-spin" id="cp_sign">
                        <label for="cp_sign">Code Postal</label>
                    </div>
                </div>

                <div class="col s12 m6 center-align">
                    <div class="input-field inline width45">
                        <i class="material-icons prefix">mail</i>
                        <input type="email" name="email" class="validate" id="email_sign">
                        <label for="email_sign">E-mail</label>
                    </div>
                </div>
                <div class="col s12 m6 center-align">
                    <div class="input-field inline">
                        <i class="material-icons prefix">lock</i>
                        <input type="password" name="password" class="validate" id="pwd1_sign">
                        <label for="pwd1_sign">Mot de passe</label>
                    </div>
                </div>

                <div class="col s12 m6 center-align">
                    <div class="input-field inline">
                        <i class="material-icons prefix">lock</i>
                        <input type="password" name="password2" class="validate" id="pwd2_sign">
                        <label for="pwd2_sign">Confirmer le mot de passe</label>
                    </div>
                </div>
                <div class="col s12 m6 center-align">
                    <div class="input-field inline width45">
                        <i class="material-icons prefix">phone</i>
                        <input type="tel" name="phone" class="validate" id="phone_sign">
                        <label for="phone_sign">Téléphone</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <a class="btn-flat modal-close waves-effect waves-red">Annuler</a>
            <button type="submit" name="submitSignUp" class="btn waves-effect waves-green">S'inscrire</button>
        </div>
    </form>
</div>