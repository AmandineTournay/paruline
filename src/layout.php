<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title><?= $title ?></title>

        <!-- CSS + FONT + ICONS DU MATERIAL DESIGN -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="../extensions/material_icons/material-icons.css">
        <!--link rel="stylesheet" href="https://font.googleapis.com/css?family=Roboto:300,400,500"-->

        <!-- CSS DU PROJET -->
        <link rel="stylesheet" href="../public/styles/styles.css">
    </head>
    <body class="userSelect">
        <?php
            include_once "view/pops/signup.php";
            include_once "view/pops/login.php";
        ?>
        <header>
            <nav class="nav-extended">
                <div class="nav-wrapper">
                    <a href="index.php" class="brand-logo center">Paruline</a>
                    <a href="#" data-target="mobileMenu" class="sidenav-trigger menuHamburger"><i class="material-icons">menu</i></a>

                    <ul class="right hide-on-med-and-down">
                        <?php
                        if (!isset($_SESSION["session"]) === true) :
                            ?>
                            <li>
                                <a href="#signUp" class="btn waves-effect waves-light modal-trigger menuBtn">S'inscrire</a>
                            </li>
                            <li>
                                <a href="#login" class="btn waves-effect waves-light modal-trigger menuBtn">Se connecter</a>
                            </li>

                            <?php
                        else :
                            ?>
                            <li><a href="controller/logout.php" class="btn waves-effect waves-light menuBtn">Se déconnecter</a></li>
                            <?php
                        endif;
                        ?>
                    </ul>

                    <ul class="sidenav" id="mobileMenu">
                        <li class="center-align logoMobile"><img src="../public/images/paruline.png" alt="Logo" width="200"></li>
                        <li class="divider"></li>

                        <li><a href="index.php?p=">Accueil</a></li>
                        <li><a href="index.php?p=creations">Les réalisations</a></li>
                        <li><a href="index.php?p=sheers">Voilages</a></li>
                        <li><a href="index.php?p=curtains">Rideaux</a></li>

                        <li class="divider"></li>

                        <?php
                        if (isset($_SESSION["session"]) == true) :
                            ?>
                            <li>
                                <ul class="collapsible">
                                    <li>
                                        <a class="collapsible-header">Achats <i class="material-icons right">arrow_drop_down</i></a>
                                        <div class="collapsible-body">
                                            <ul>
                                                <li>
                                                    <a href="index.php?p=buy&menu=">Accueil Achats</a>
                                                </li>
                                                <li>
                                                    <a href="index.php?p=buy&menu=userInfos">Informations <i class="material-icons right">info</i></a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="index.php?p=buy&menu=buyAccessories">Accessoires</a>
                                                </li>
                                                <li>
                                                    <a href="index.php?p=buy&menu=tailor-made">Sur-Mesure</a>
                                                </li>
                                                <li class="divider"></li>
                                                <?php
                                                    if ($_SESSION["id_user"] == 1) :
                                                        ?>
                                                        <li>
                                                            <a href="index.php?p=buy&menu=providers">Liste des fournisseurs</a>
                                                        </li>
                                                        <li>
                                                            <a href="index.php?p=buy&menu=customers">Liste des clients</a>
                                                        </li>
                                                        <li>
                                                            <a href="index.php?p=buy&menu=orderList">Liste des commandes</a>
                                                        </li>
                                                        <li class="divider"></li>
                                                        <li>
                                                            <a href="index.php?p=buy&menu=accessPrices">Prix des accessoires</a>
                                                        </li>
                                                        <?php
                                                    else :
                                                        ?>
                                                        <li>
                                                            <a href="index.php?p=buy&menu=cart">Panier <i class="material-icons right">shopping_cart</i></a>
                                                        </li>
                                                    <?php endif; ?>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                            <li class="divider"></li>

                            <li><a href="controller/logout.php" class="btn waves-effect waves-light">Se déconnecter</a></li>
                        <?php
                        else :
                            ?>
                            <li>
                                <a href="#signUp" class="btn waves-effect waves-light modal-trigger">S'inscrire</a>
                            </li>
                            <li>
                                <a href="#login" class="btn waves-effect waves-light modal-trigger">Se connecter</a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </nav>
        </header>

        <section class="center-align z-depth-2 sticky-nav">
            <a href="index.php?p=">Accueil</a>
            <a href="index.php?p=creations">Les réalisations</a>
            <a href="index.php?p=sheers">Les voilages</a>
            <a href="index.php?p=curtains">Les rideaux</a>
            <?php
                if (isset($_SESSION['session']) === true) :
                    ?>
                    <a href="index.php?p=buy">Boutique</a>
                <?php
                endif;
            ?>
        </section>

        <div class="container fullCol">
            <div class="row flexWrap">
                <?php
                    if (isset($_GET['p']) && $_GET['p'] == 'buy') :
                        ?>
                        <section class="col l2 valign-wrapper sideMenu userSelect">
                            <div class="centerAuto">
                                <a class="sideItem" href="index.php?p=buy&menu=">
                                    <p class="menuItemName">Accueil Achats</p>
                                </a>
                                <a class="sideItem" href="index.php?p=buy&menu=userInfos">
                                    <p class="menuItemName">Informations<i class="material-icons right">info</i></p>
                                </a>

                                <a class="sideItem" href="index.php?p=buy&menu=buyAccessories">
                                    <p class="menuItemName">Accessoires</p>
                                </a>
                                <a class="sideItem" href="index.php?p=buy&menu=tailor-made">
                                    <p class="menuItemName">Sur-Mesure</p>
                                </a>

                                <br>

                                <?php
                                    if ($_SESSION["id_user"] == 1) :
                                        ?>
                                        <a class="sideItem" href="index.php?p=buy&menu=providers">
                                            <p class="menuItemName">Liste des fournisseurs</p>
                                        </a>
                                        <a class="sideItem" href="index.php?p=buy&menu=customers">
                                            <p class="menuItemName">Liste des clients</p>
                                        </a>
                                        <a class="sideItem" href="index.php?p=buy&menu=orderList">
                                            <p class="menuItemName">Liste des commandes</p>
                                        </a>

                                        <a class="sideItem" href="index.php?p=buy&menu=accessPrices">
                                            <p class="menuItemName">Prix des accessoires</p>
                                        </a>
                                    <?php
                                    else :
                                        ?>
                                        <a class="sideItem" href="index.php?p=buy&menu=cart">
                                            <p class="menuItemName">Panier<i class="material-icons right">shopping_cart</i></p>
                                        </a>
                                    <?php
                                    endif;
                                ?>
                            </div>
                        </section>
                    <?php
                    endif;
                ?>

                <main class="col <?= isset($_GET['p']) && $_GET['p'] == 'buy' ? 'l10 mainContent' : 'mainContent'; ?>">
                    <?= $content; ?>
                </main>
            </div>
        </div>

        <!-- SCRIPTS -->
            <!-- SCRIPT Materialize -->
        <script type="text/javascript" src="../extensions/materialize/js/materialize.js"></script>

            <!-- SCRIPT JQUERY -->
        <script src="../extensions/jquery-3.3.1.min.js"></script>

            <!-- SCRIPTS DU PROJET -->
        <script src="../public/js/layout.js"></script>
        <script src="../public/js/functions.js"></script>
        <script src="../public/js/userInfos.js"></script>
        <script src="../public/js/redirectCart.js"></script>
        <script src="../public/js/updateAccessories.js"></script>
        <script src="../public/js/updatePriceA.js"></script>
        <script src="../public/js/tailor-made.js"></script>
    </body>
</html>