<?php
    session_start();

    if (!isset($_GET['p']) || isset($_GET['p']) == "" || empty($_GET['p'])) {
        $page = "home";
    }
    else {
        if (!file_exists("view/" . $_GET['p'] . ".php")) {
            $page = "404";
        }
        else {
            $page = $_GET['p'];
        }
    }

    ob_start();

    include_once "view/" . $page . ".php";
    $content = ob_get_contents();

    ob_end_clean();

    include_once "layout.php";